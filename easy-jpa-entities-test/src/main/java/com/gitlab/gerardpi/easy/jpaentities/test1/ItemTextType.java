package com.gitlab.gerardpi.easy.jpaentities.test1;

public enum ItemTextType {
    NAME,
    TITLE,
    SUBTITLE,
    DESCRIPTION,
    REMARKS,
    SUMMARY
}
