package com.gitlab.gerardpi.easy.jpaentities.test1;

public final class SpringProfile {
    public static final String PROD = "prod";
    public static final String TEST = "test";
}
