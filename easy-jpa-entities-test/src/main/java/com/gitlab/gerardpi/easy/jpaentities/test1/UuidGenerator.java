package com.gitlab.gerardpi.easy.jpaentities.test1;

import java.util.UUID;

@FunctionalInterface
public interface UuidGenerator {
    UUID generate();
}
