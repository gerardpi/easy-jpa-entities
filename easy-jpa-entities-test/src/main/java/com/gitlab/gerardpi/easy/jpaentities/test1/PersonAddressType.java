package com.gitlab.gerardpi.easy.jpaentities.test1;

public enum PersonAddressType {
    RESIDENCE,
    PROPERTY
}
