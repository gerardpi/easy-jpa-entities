package com.gitlab.gerardpi.easy.jpaentities.processor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.gitlab.gerardpi.easy.jpaentities.processor.entitydefs.EasyJpaEntitiesConfig;
import com.gitlab.gerardpi.easy.jpaentities.processor.entitydefs.EntityClassDef;

import javax.annotation.processing.ProcessingEnvironment;
import java.io.IOException;
import java.io.Reader;
import java.io.UncheckedIOException;

import static com.gitlab.gerardpi.easy.jpaentities.processor.ProcessorUtils.error;

public final class PersistableDefsDeserializer {
    private PersistableDefsDeserializer() {
        // No instantation
    }
    public static ObjectMapper createYamlObjectMapper() {
        YAMLFactory yamlFactory = new YAMLFactory();
        return new ObjectMapper(yamlFactory);
    }

    static EasyJpaEntitiesConfig slurpFromYaml(Reader reader, String yamlFileName, ProcessingEnvironment procEnv) {
        try {
            EasyJpaEntitiesConfig easyJpaEntitiesConfig = createYamlObjectMapper().readValue(reader, EasyJpaEntitiesConfig.class);
            ProcessorUtils.note(procEnv, "Loading persistable defs reading file '" + yamlFileName + "'");
            ProcessorUtils.note(procEnv, "It contains " + easyJpaEntitiesConfig.getEntityClassDefNames().size() + " persistable class defs: " + easyJpaEntitiesConfig.getEntityClassDefNames());
            return easyJpaEntitiesConfig;
        } catch (IOException e) {
            ProcessorUtils.error(procEnv, "Error loading persistable defs from file '" + yamlFileName + "': " + e.getMessage());
            throw new UncheckedIOException(e);
        }
    }

    static EntityClassDef slurpEntityClassDefFromYaml(Reader reader, String yamlFileName, ProcessingEnvironment procEnv) {
        try {
            EntityClassDef entityClassDef = createYamlObjectMapper().readValue(reader, EntityClassDef.class);
            ProcessorUtils.note(procEnv, "Loaded entity class def reading file '" + yamlFileName + "'");
            return entityClassDef;
        } catch (IOException e) {
            ProcessorUtils.error(procEnv, "Error loading entity class def from file '" + yamlFileName + "': " + e.getMessage());
            throw new UncheckedIOException(e);
        }
    }
}
